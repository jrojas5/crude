<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PageController@index');

Route::get('/detalle/{id}', 'PageController@detalle')->name('detalle');

Route::post('/', 'PageController@crear')->name('crear');

Route::get('/editar/{id}', 'PageController@editar' )->name('editar');

Route::put('/editar/{id}', 'PageController@update' )->name('update');

Route::delete('/eliminar/{id}', 'PageController@eliminar')->name('eliminar');