<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class PageController extends Controller
{
    public function index(){
        $miembros = App\Nota::all();
        return view('miembros',compact('miembros'));
    }

    public function detalle($id){

        // $nota = App\Nota::find($id);
    
        //Aquí valida si existe sino redirije al 404
        $nota = App\Nota::findOrFail($id);
    
        return view('detalle', compact('nota'));
    }

    public function crear(Request $request){
        // return $request->all();
        
        $request->validate([
            'nombre' => 'required',
            'apellido' => 'required'
        ]);

        $notaNueva = new App\Nota;
        $notaNueva->nombre = $request->nombre;
        $notaNueva->apellido = $request->apellido;

        $notaNueva->save();

        return back()->with('mensaje', 'Miembro Agregado!');
    }

    public function editar($id){

        $nota = App\Nota::findOrFail($id);
        return view('editar', compact('nota'));
        
    }

    public function update(Request $request, $id){
        
        $request->validate([
            'nombre' => 'required',
            'apellido' => 'required'
        ]);
        
        $notaActualizada = App\Nota::find($id);
        $notaActualizada->nombre = $request->nombre;
        $notaActualizada->apellido = $request->apellido;
        
        $notaActualizada->save();

        return redirect('/')->with('mensaje2', 'Miembro Editado!');
        // return redirect('/');
    }

    public function eliminar($id){

        $notaEliminar = App\Nota::findOrFail($id);
        $notaEliminar->delete();
    
        return back()->with('mensaje3', 'Miembro Eliminado!');
    }
}
